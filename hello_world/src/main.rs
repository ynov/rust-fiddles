fn print_number(x: i32) {
    println!("x is: {}", x);
}

fn print_sum(x: i32, y: i32) {
    println!("sum is: {}", x + y);
}

fn add_one(x: i32) -> i32 {
    x + 1
    // no semicolon
}

// There are two kinds of statements in Rust:
// - 'declaration statements' and
// - 'expression statements'

fn main() {
    println!("Hello, world!");

    // Functions
    print_number(10);
    print_sum(10, 5);
    println!("add_one(5) is: {}", add_one(5));

    println!("");

    // Arrays
    let mut a1 = [0; 10]; // a1: [i32, 10]
    a1[0] = 128;
    a1[1] = 256;
    println!("a1 has {} elements", a1.len());
    println!("a1[0] = {}, a1[1] = {}, a1[2] = {}", a1[0], a1[1], a1[2]);

    let names = ["Graydon", "Brian", "Niko"];
    println!("The second name is: {}", names[1]);

    // Slices
    // let a2 = [0, 1, 2, 3, 4];
    // let middle = &a2[1..4];
    // let complete = &a2[..];

    let st1 = "The quick brown fox jumps over the lazy dog";
    let qbf = &st1[4..19];
    println!("qbf: {}", qbf);

    // Tuples
    let tup1 = (1, "hello");
    println!("tup1.0 = {}, tup1.1 = {}", tup1.0, tup1.1);
    let (tda, tdb) = tup1;
    println!("tda = {}, tdb = {}", tda, tdb);

    // Functions (Primitive Types)
    fn foo(x: i32) -> i32 { x }
    let fx: fn(i32) -> i32 = foo;
    println!("fx(50) = {}", fx(50));
}
